import axios from "axios";

export function getFollowers(token) {
  return axios
    .get("http://localhost:8081/api/Social/followers", {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      console.log(error.response);
      return null;
    });
}

export function getFollowing(token) {
  return axios
    .get("http://localhost:8081/api/Social/followers", {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      console.log(error.response);
      return null;
    });
}
