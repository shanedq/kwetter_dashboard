import axios from "axios";

export function getKweets(token) {
  return axios
    .get("http://localhost:8083/api/Post/kweets", {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      console.log(error.response);
      return null;
    });
}
