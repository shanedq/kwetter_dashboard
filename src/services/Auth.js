export function decodeJWT(token) {
  var base64url = token.split(".")[1];
  var base64 = base64url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
  return JSON.parse(jsonPayload);
}

export function getTken() {
  return sessionStorage.getItem("token");
}

export function login(token) {
  sessionStorage.setItem("token", token);
  console.log("Token set");
}

export function logout() {
  sessionStorage.removeItem("token");
}
