import React from "react";
import "./Navbar.css";
import keycloakjs from "../../keycloak";

export default function Navbar() {
  const keycloak = keycloakjs;

  function LogoutUser() {
    keycloak.logout({ redirectUri: "http://localhost:3000" });
  }

  return (
    <div className="navbar-container">
      <ul>
        <li>
          <a href="#home">Home</a>
        </li>
        <li>
          <a href="#profile">Profile</a>
        </li>
        <li>
          <button onClick={LogoutUser}>Logout</button>
        </li>
      </ul>
      <div className="img-container"></div>
    </div>
  );
}
