import React from "react";
import "./User.css";

export default function User(props) {
  return (
    <div className="user-container">
      <div className="img-container"></div>
      <div className="name-container">
        <h3>{props.user[0].name}</h3>
      </div>
      <div className="details-container">
        <p>Location: {props.user[0].location}</p>
        <p>Web: {props.user[0].web}</p>
        <p>Bio: {props.user[0].bio}</p>
      </div>
    </div>
  );
}
