import React from "react";
import "./Feed.css";
import Kweet from "./Kweet";

export default function Feed(props) {
  console.log("feed: ", props);
  return (
    <div className="feed-container">
      <div className="content">
        {props.kweets.map((kweet, index) => (
          <div className="kweet" key={index}>
            <Kweet kweet={kweet} />
          </div>
        ))}
      </div>
    </div>
  );
}
