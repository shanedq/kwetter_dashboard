import React from "react";
import "./Kweet.css";

export default function Kweet(props) {
  return (
    <div className="kweet-container">
      <div className="name">
        <p>{props.kweet.name}</p>
      </div>
      <div className="kweet">
        <p>{props.kweet.message}</p>
      </div>
      <div className="likes">
        <p>{props.kweet.likes}</p>
      </div>
    </div>
  );
}
