import "./App.css";
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import Login from "./pages/login/Login.js";
import Profile from "./pages/profile/Profile.js";
import Navbar from "./components/navbar/Navbar";

function App() {
  return (
    <div className="wrapper">
      <BrowserRouter>
        <ul>
          <li>
            <Link to="/">public component</Link>
          </li>
          <li>
            <Link to="/profile">secured component</Link>
          </li>
        </ul>
        <Switch>
          <Route exact path="/" component={LoginContainer} />
          <Route exact path="/profile" component={DefaultContainer} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

const LoginContainer = () => (
  <div className="container">
    <Route path="/" component={Login} />
  </div>
);

const DefaultContainer = () => (
  <div className="container">
    <Navbar />
    <Route exact path="/profile" component={Profile} />
  </div>
);

export default App;
