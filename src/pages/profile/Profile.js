import React, { useEffect, useState, Component } from "react";
import "./Profile.css";
import User from "../../components/profile/User";
import Feed from "../../components/feed/Feed";
import Followers from "../../components/profile/Followers";
import keycloakjs from "../../keycloak";
import { login, getTken, decodeJWT, logout } from "../../services/Auth";
import { getFollowers, getFollowing } from "../../services/FollowerService";
import { getKweets } from "../../services/PostService";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.user,
      kweets: props.kweets,
      followers: props.followers,
      keycloak: null,
      authenticated: false,
    };
  }

  componentDidMount() {
    const keycloak = keycloakjs;
    keycloak
      .init({ onLoad: "login-required" })
      .then((authenticated) => {
        login(keycloak.token);
        this.setState({ keycloak: keycloak, authenticated: authenticated });
      })
      .then(() => {
        this.setUserData();
        this.setKweetsData(keycloak.token);
        this.setFollowersData(keycloak.token);
      });
  }

  setUserData = () => {
    const data = [
      {
        id: 0,
        name: "Shane",
        location: "There",
        web: "shane@live.nl",
        bio: "I like to fly on my horse!",
      },
    ];
    this.setState({ user: data });
  };

  setKweetsData = (token) => {
    const data = [
      {
        id: 0,
        user: "Shane",
        text: "I just started using Kwetter",
        nrOfLikes: 4,
        createdAt: "2021-04-15T13:25:06",
      },
      {
        id: 1,
        user: "Shane",
        text: "I like Kweeting!",
        nrOfLikes: 7,
        createdAt: "2021-04-15T13:29:06",
      },
      {
        id: 2,
        user: "Shane",
        text: "I don't like it anymore >:(",
        nrOfLikes: 10,
        createdAt: "2021-04-15T13:54:06",
      },
      {
        id: 3,
        user: "Shane",
        text: "I go make some Bic Mac soup now.",
        nrOfLikes: 1,
        createdAt: "2021-04-15T14:54:06",
      },
      {
        id: 4,
        user: "Shane",
        text: "I ate my Bic Mac soup, okay.",
        nrOfLikes: 5,
        createdAt: "2021-04-15T15:21:06",
      },
    ];
    getKweets(token).then((res) => {
      this.setState({ kweets: res });
    });
  };

  setFollowersData = (token) => {
    const followers = {
      followers: 0,
      following: 0,
    };
    getFollowers(token).then((res) => {
      followers.followers = res.length;

      getFollowing(token).then((res) => {
        followers.following = res.length;
        this.setState({ followers: followers });
      });
    });
  };

  render() {
    if (this.state.keycloak) {
      if (this.state.authenticated)
        return (
          <div className="page-container">
            <div>
              {this.state.user != null ? <User user={this.state.user} /> : null}
            </div>
            <div>
              {this.state.kweets != null ? (
                <Feed kweets={this.state.kweets} />
              ) : null}
            </div>
            <div>
              {this.state.followers != null ? (
                <Followers followers={this.state.followers} />
              ) : null}
            </div>
          </div>
        );
      else return <div>Unable to authenticate</div>;
    }
    return <div>Initializing Keycloak...</div>;
  }
}
export default Profile;
