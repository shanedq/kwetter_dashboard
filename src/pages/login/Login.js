import React from "react";
import { useHistory } from "react-router-dom";
import "./Login.css";

export default function Login() {
  const history = useHistory();

  function handleOnSubmit() {
    history.push("/profile");
  }

  return (
    <div className="login-container">
      <div className="login-wrapper">
        <div>
          <h1>
            <span className="fit">Kwetter</span>
          </h1>
          <form onSubmit={handleOnSubmit}>
            <label>
              <p>
                <input
                  className="username-input"
                  placeholder="name@domain.com"
                  type="text"
                />
              </p>
            </label>
            <label>
              <p>
                <input
                  className="password-input"
                  placeholder="password"
                  type="password"
                />
              </p>
            </label>
            <div>
              <button className="submit-btn" type="submit">
                Submit
              </button>
            </div>
            <div className="footer">
              No account yet?
              <a className="register-link" href="http://localhost:3000/">
                Register
              </a>
              <p>
                <a href="http://localhost:3000/">Forgot password?</a>
              </p>
            </div>
          </form>
        </div>
      </div>
      <div className="welcome-wrapper">
        <div className="img-container"></div>
      </div>
    </div>
  );
}
