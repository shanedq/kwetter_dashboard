import Keycloak from "keycloak-js";

const initOptions = {
  url: "http://localhost:8082/auth",
  realm: "Kwetter",
  clientId: "my-react-client",
  onLoad: "login-required",
};

const keycloak = new Keycloak(initOptions);

export default keycloak;
